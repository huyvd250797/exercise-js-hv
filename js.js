// TODO ------------------------------- Exercise 01: Check Letter ------------------------------ */

//* 1. Check number
//* - is Number?
//* - Số nguyên (Integer)
//* - Số thực (Float)
//* - Số dương (Positive)
//* - Số âm (Negative)
//* - Số nguyên tố (Prime)

// Check is Number?
function isNumber(num) {
  if (isNaN(num) == false) {
    return true;
  } else {
    return false;
  }
}

// Check Positive Number
function isPositive(num) {
  // positiveNum > 0 ? console.log(positiveNum,'is Positive Number') : positiveNum < 0 ? console.log(positiveNum,'is Negative Number') : console.log(positiveNum,'is Zero')
  if (isNumber(num) == true) {
    if (num > 0) {
      return true;
    } else if (num < 0) {
      return false;
    } else if (num == 0);
    return 0;
  } else {
    return false;
  }
}

// Check Integer Number
function isinteger(num) {
  // integerNum == 0 ? console.log(integerNum,'is Zero') : integerNum % 1 == 0 ? console.log(integerNum,'is integer Number') : console.log(integerNum,'is Float Number')
  if (isNumber(num) == true) {
    if (num == 0) {
      return 0;
    } else if (num % 1 == 0) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

// Check Prime Number
function isPrime(num) {
  // gán mặc định cho biến = true (là số nguyên tố)
  let primeNum = true;
  if (num < 2) {
    primeNum = false;
  } else {
    for (let i = 2; i < num; i++) {
      if (num % i == 0) {
        primeNum = false;
      }
    }
  }
  return primeNum;
}

let number = 73;
console.log(number, "is Number? ➜", isNumber(number));
console.log(number, "is Positive Number? ➜", isPositive(number));
console.log(number, "is Integer Number? ➜", isinteger(number));
console.log(number, "is Prime Number? ➜", isPrime(number));

console.log("\n");

// TODO ------------------------------- Exercise 02: Calculator ------------------------------ */

//* 1. Tính tổng 2 số hạng
// Sum Number (2 number)
function sumNumber(num1, num2) {
  if (isNumber(num1) == true && isNumber(num2)) {
    let sumNum = Number(num1) + Number(num2);
  } else {
    return false;
  }
  return sumNum;
}

let number01 = "a";
let number02 = 3;
console.log(
  "Sum:",
  number01,
  "+",
  number02,
  "=",
  sumNumber(number01, number02)
);

console.log("\n");

// TODO ------------------------------- Exercise 03: Array ------------------------------ */

//* 1. Tính tổng các phần tử trong mảng

// Tổng các phần tử mảng
function sumArray(arr) {
  let sumArr = 0;
  for (let i = 0; i < arr.length; i++) {
    // Check từng phần tử có phải Number?
    if (isNumber(arr[i]) == true) {
      // Chuyển arr[i] từ String -> Number // Nếu không hệ thống sẽ cộng chuỗi
      sumArr += Number(arr[i]);
    } else {
      console.log(
        "Element",
        arr[i],
        "( position:",
        i,
        ") is not Number ➜ FALSE"
      );
      // // Tìm thấy ký tự không phải Number ➜ Break: thoát vòng lặp
      // break;
    }
  }
  return sumArr;
}

let array = ["a", 0, 1, 2, 3, 4, 5, 6, 7, "c", "7"];

console.log("Array:", array);
console.log("Total Value Number of Array:", sumArray(array));

console.log("\n");

// TODO ------------------------------- Exercise 04: Date Time ------------------------------ */

// Hàm get thứ
function getDay2(theDay) {
  var dayList = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  for (let i = 0; i < dayList.length; i++) {
    if (theDay - 1 == i) {
      return dayList[i];
    }
  }
}

// hàm get Month -> truyền vào 1 số từ 0 -> 11 (tương ứng tháng 1 -> tháng 12)
function getMonth(month) {
  var monthList = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  for (let i = 0; i < monthList.length; i++) {
    if (month == i) {
      return monthList[i];
    }
  }
}

//* 1. Write a JavaScript program to display the current day and time in the following format.
// Sample Output : Today is : Tuesday.
// Current time is : 10 : 30 : 38 PM

function getCurrentDateTime() {
  // new Date(): lấy thời gian hiện tại
  var today = new Date();

  // getDay(): lấy giá trị ngày trong tuần (0: sunday -> 8: Saturday)
  var day = today.getDay();

  // Array Day
  var dayList = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  // get Day -> day = 5 => dayList[5] = Friday
  console.log("Today is:", dayList[day]);

  // getHours: lấy giá trị giờ
  var hour = today.getHours();

  // getHours: lấy giá trị giờ
  var minute = today.getMinutes();

  // getHours: lấy giá trị giờ
  var second = today.getSeconds();

  // Nếu giờ lớn hơn 12 => PM
  var prepand = hour >= 12 ? "PM" : "AM";

  // Chuyển định dạng 24h thành 12h
  hour = hour >= 12 ? hour - 12 : hour;

  // nếu số giờ < 10 => thêm 0 đăng trước
  hour = hour < 10 ? "0" + hour : hour;

  console.log(
    "Current time is:",
    ":",
    hour + ":" + minute + ":" + second,
    prepand
  );
}

getCurrentDateTime();

//* 2. Write a JavaScript program to find out if Day, Month will be a Date (Sun, Mon, Tue,...) between year to year.

// hàm findDay: tìm thứ ngày tháng trong các năm xem có bao nhiêu năm có các dữ liệu đã nhập
function findDay(theDay, theDate, theMonth, fromYear, toYear) {
  // Add 0 before the Month < 10 ==> 7 --> 07
  theMonth < 10 ? (theMonth = "0" + theMonth) : theMonth;

  // Printf data
  console.log(
    "Day:",
    theDate,
    "Month:",
    theMonth,
    "from",
    fromYear,
    "-",
    toYear,
    "is being a",
    getDay2(theDay)
  );

  // trừ 1 do nhập dữ liệu tháng 7 => phần tử thứ 7 là tháng 8
  theMonth -= 1;

  for (let year = fromYear; year <= toYear; year++) {
    // getDate = Lấy dữ liệu ngày tháng năm theo dữ liệu đã nhập
    let getDate = new Date(year, theMonth, theDate);

    // Nếu dữ liệu ngày của getDate = theDay đã nhập thì in ra dữ liệu kết quả
    if (getDate.getDay() === theDay) {
      console.log(getDate);
    }
  }
}

// Nhập theo cấu trúc:
// Thứ - ngày - tháng - từ năm - đến năm
findDay(4, 25, 7, 2024, 2050);

//* 3. Write a JavaScript program to calculate the days left before the someday.
function countDown(day, month, year) {
  var today = new Date();
  // getFullYear() -> lấy năm hiện tại
  // month - 1 -> nhập tháng 12 -> hệ thống sẽ tính từ 0 -> = 11
  // 25 -> ngày 25
  var countDown = new Date(today.getFullYear(), month - 1, day);

  //? Check if the current date is after year/month/date
  //! fixing...
  if (
    today.getMonth() >= month - 1 &&
    today.getDate() > day &&
    year > today.getFullYear()
  ) {
    // If true, set the next year
    countDown.setFullYear(
      countDown.getFullYear() + (year - countDown.getFullYear())
    );
  }

  //! Calculate the difference in days between today and day inout -- Chưa hiểu? -> 1000 * 60 * 60 * 24 ???
  var one_day = 1000 * 60 * 60 * 24;
  // month < 10 ? (month = "0" + month) : month;
  // day < 10 ? (day = "0" + day) : day;

  // Log the number of days left until Christmas to the console
  console.log(
    Math.ceil((countDown.getTime() - today.getTime()) / one_day),
    "days left until",
    day + "/" + month + "/" + year
  );
}

countDown(12, 12, 2025);

console.log("\n");

// TODO ------------------------------- Exercise 05: Geometry ------------------------------ */
//* 1. Write a JavaScript program to find the area of a triangle where three sides are.

function areaTriangle(side1, side2, side3) {
  // Calculate the semi-perimeter of the triangle (nửa chu vi)
  var s = (side1 + side2 + side3) / 2;
  // Use Heron's formula to calculate the area of the triangle
  var area = Math.sqrt(s * ((s - side1) * (s - side2) * (s - side3)));
  console.log(
    "Area of triangle where three sides are",
    side1 + "," + side2 + "," + side3,
    "is:",
    area.toFixed(2)
  );
}

areaTriangle(5, 6, 7);
